# !/usr/anaconda3/bin python
# -*-coding:utf-8-*-
# author:tangchaolizi
# datetime:2019-09-11 20:16
# software:PyCharm

"""
Waring:请勿随意删除以下任意条目
"""
#  登陆相关
chrome_driver_path = '/Users/tangchaolizi/Downloads/wx/chromedriver'  # ChromeDriver绝对路径

#  百度翻译相关 需在百度开放平台注册账号 http://api.fanyi.baidu.com/
translate_appid = 'translate_appid'  # 百度翻译appid
translate_secretKey = 'translate_secretKey'  # 百度翻译secretKey

#  数据库相关-Mysql
#  在数据库内执行wechat.sql
mysql_host = 'localhost'  # mysql数据库IP
mysql_username = 'mysql_username'  # mysql数据库用户名
mysql_password = 'mysql_password'  # 数据库密码
mysql_database = 'mysql_database'  # 数据库名

# Wordpress相关
wp = True  # 是否开启上传至wp True:开启 False:关闭
wp_url = 'wp_url'  # wordpress XMLRPC地址
wp_username = 'wp_username'  # wordpress 后台用户名
wp_password = 'wp_password'  # wordpress 后台密码

# 微信相关,需在微信开发者平台创建账号 https://mp.weixin.qq.com/
cookie_file = open('cookie.json', 'r')
cookies_token = eval(cookie_file.read())
wx_username = 'wx_username'  # 微信用户名
wx_password = 'wx_password'  # 微信密码
wx_token = cookies_token['token'].replace('"','')  # 微信token
wx_cookies = cookies_token['cookies']  # 微信cookies

# 阿里云OSS相关
oss_appid = 'oss_appid'  # 阿里云Appid
oss_secretkey = 'oss_secretkey'  # 阿里云secretkey
oss_url = 'oss_url'  # 阿里云oss地址
oss_bucket = 'oss_bucket'  # 阿里云bucket名称
oss_save_url = 'oss_save_url'
# 阿里云文件保存URL 如:http://test.oss-cn-beijing.aliyuncs.com/
oss_save_dir = 'img'  # 阿里云文件保存目录,如:img
oss_save_path = oss_save_url + oss_save_dir  # 阿里云oss最终保存路径
