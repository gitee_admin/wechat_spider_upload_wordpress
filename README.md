# wechat_spider_upload_wordpress

#### 介绍
将微信文章爬取，翻译成英文，繁体中文(伪原创)并上传至wp，  
参考了宋县锋大神的[WeChat_Article](https://gitee.com/songxf1024/WeChat_Article)

#### 软件架构
1. pymysql操作Mysql存储
2. wordpress官方提供XMLRPC'wordpress_xmlrpc'上传至WP
3. selenium获取用户cookies(需扫码)
4. oss2图片上传至阿里云


#### 安装教程

`pip install -r requirements.txt`  
导入数据库 wechat.sql

#### 使用说明

1.  查看并修改config.py文件
2.  `python login.py`模拟登陆获取cookies
3.  `python main.py`运行主程序开始爬取

#### 参与贡献

1. [WeChat_Article](https://gitee.com/songxf1024/WeChat_Article)
