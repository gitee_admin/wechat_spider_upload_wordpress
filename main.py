# !/usr/anaconda3/bin python
# -*-coding:utf-8-*-
# author:tangchaolizi
# datetime:2019-09-10 13:50
# software:PyCharm

import WX_Article
import pymysql
import requests
import random
import time
import config
from hashlib import md5
from wordpress_xmlrpc import Client, WordPressPost
from wordpress_xmlrpc.methods.posts import NewPost
import json


class Run(object):
    def __init__(self, query_name):
        """

        :param query_name: 公众号
        """
        self.conn = pymysql.connect(host=config.mysql_host,
                                    user=config.mysql_username,
                                    password=config.mysql_password,
                                    database=config.mysql_database)
        self.cursor = self.conn.cursor()
        self.token = config.wx_token
        self.cookies = json.loads(r'{}'.format(config.wx_cookies))
        self.username = config.wx_username
        self.pwd = config.wx_password
        self.query_name = query_name

    def wechat(self):
        """
        运行微信爬虫
        :return:
        """
        wechat_spider = WX_Article.GetAriticle(self.token, self.cookies, self.query_name)
        try:
            fakeid = wechat_spider.Get_WeChat_Subscription()
            wechat_spider.Get_Articles(fakeid)
        except Exception as Error:
            print(Error)
            error.append(self.query_name)
            pass

    def post_article(self, category, title, content, img_content):
        """
        上传至Wordpress
        :param category: 文章分类
        :param title: 文章标题
        :param content:  文章内容
        :param img_content: 文章图片
        :return:
        """
        content = content + '\n' + img_content  # 内容与图片拼接
        client = Client(config.wp_url, config.wp_username, config.wp_password)
        post = WordPressPost()
        post.title = title
        post.content = content
        post.post_status = 'publish'
        post.terms_names = {'category': [category], 'post_tag': [self.query_name]}
        client.call(NewPost(post))  # 上传

    def post_wp(self):
        """
        取出文章并翻译
        :return:
        """
        sql = "select title, content, img from wechat WHERE status='1';"
        # 数据库状态1为未上传文章，0为已上传文章，取出未上传文章
        self.cursor.execute(sql)
        result = self.cursor.fetchall()
        for i in result:
            #  取出未上传文章
            try:
                img_content = ''
                print('*' * 60)
                translate_title_en = self.translate(i[0], 'zh', 'en')  # 标题简体中文转英文
                translate_title_zh = self.translate(translate_title_en, 'en', 'zh')  # 标题英文转简体中文(此结果将上传至WP)
                translate_title_cht = self.translate(translate_title_zh, 'zh', 'cht')  # 标题中文转繁体中文
                title = translate_title_zh
                print(len(i[1]))
                print(title)
                img_item = i[2].split(', ')  # 取出图片
                for img in img_item:
                    # 拼接图片
                    img_content = img_content + '<figure class="wp-block-image">' \
                                                '<img src="{}/{}.jpeg" alt=""/>' \
                                                '</figure>'.format(config.oss_save_path, img) + '\n'
                translate_content_spa = self.translate(i[1], 'zh', 'spa')
                translate_content_en = self.translate(translate_content_spa, 'spa', 'en')  # 内容简体中文转英文
                translate_content_zh = self.translate(translate_content_en, 'en', 'zh')  # 内容英文转简体中文
                translate_content_cht = self.translate(translate_content_zh, 'zh', 'cht')  # 内容简体中文转繁体中文
                self.post_article('娱乐', translate_title_zh, translate_content_zh, img_content)  # 上传至相关分类
                self.post_article('entertainment', translate_title_en, translate_content_en, img_content)
                self.post_article('娛樂', translate_title_cht, translate_content_cht, img_content)
                sql = "update wechat set status='0' where title='{}';".format(i[0])  # 修改当前文章状态
                self.cursor.execute(sql)
                self.conn.commit()
            except Exception as Error:
                print(len(i[1]))
                print(Error)
                continue
        self.conn.close()

    def translate(self, q, fromlang, tolang):
        """
        调用百度翻译API自动翻译内容(伪原创)
        :param q:翻译内容
        :param fromlang:翻译内容语言
        :param tolang:输出语言
        :return: result
        """
        data = {}
        translate_appid = config.translate_appid
        translate_secretkey = config.translate_secretKey
        translate_result = ""
        salt = random.randint(32768, 65536)
        sign = translate_appid + q + str(salt) + translate_secretkey
        m1 = md5()
        m1.update(sign.encode('utf-8'))
        sign = m1.hexdigest()
        data['appid'] = translate_appid
        data['q'] = q
        data['from'] = fromlang
        data['to'] = tolang
        data['salt'] = str(salt)
        data['sign'] = sign
        url = 'http://api.fanyi.baidu.com/api/trans/vip/translate/'
        translate_get = requests.post(url=url, data=data)  # 以上均按照百度翻译官方格式编写并访问
        translate_content = eval(translate_get.text)['trans_result']
        for i in translate_content:
            # 拼接输出内容
            translate_result += i['dst'] + '\n'
        time.sleep(1)
        return translate_result


if __name__ == '__main__':
    error = []
    writer_items = open('writer.txt', 'r', encoding='utf-8')
    writer_items = eval(writer_items.read())
    for writer in writer_items:
        test = Run(writer)
        test.wechat()
        if config.wp:
            test.post_wp()
    error_file = open('error.txt', 'a+')
    error_file.write(str(error))
