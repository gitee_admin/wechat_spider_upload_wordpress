# !/usr/anaconda3/bin python
# -*-coding:utf-8-*-
# author:tangchaolizi
# datetime:2019-09-12 20:48
# software:PyCharm
import re
import json
import config
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By

item = {}
browser = webdriver.Chrome(executable_path=config.chrome_driver_path)
browser.maximize_window()
browser.get(r'https://mp.weixin.qq.com')
browser.implicitly_wait(60)
account = browser.find_element_by_name("account")
password = browser.find_element_by_name("password")
account.click()
account.send_keys(config.wx_username)
password.click()
password.send_keys(config.wx_password)
browser.find_element_by_xpath(r'//*[@id="header"]/div[2]/div/div/div[1]/form/div[4]/a').click()
WebDriverWait(browser, 60 * 10, 0.5).until(
    expected_conditions.presence_of_element_located((By.CSS_SELECTOR, r'.weui-desktop-account__nickname'))
)
print("登陆成功")
token = re.search(r'token=(.*)', browser.current_url).group(1)
cookies = browser.get_cookies()
item['cookies'] = json.dumps(cookies)
item['token'] = json.dumps(token)
with open("cookie.json", 'w+') as fp:
    fp.write(str(item))
    fp.close()
browser.close()
print('token:', token)
