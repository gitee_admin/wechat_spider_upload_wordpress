/*
 Navicat Premium Data Transfer

 Source Server         : aliyun
 Source Server Type    : MariaDB
 Source Server Version : 100121
 Source Host           : localhost:3306
 Source Schema         : article

 Target Server Type    : MariaDB
 Target Server Version : 100121
 File Encoding         : 65001

 Date: 02/03/2020 23:40:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for wechat
-- ----------------------------
DROP TABLE IF EXISTS `wechat`;
CREATE TABLE `wechat` (
  `id` INT AUTO_INCREMENT PRIMARY KEY,
  `title` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `content` longtext COLLATE utf8_bin,
  `img` varchar(5000) COLLATE utf8_bin DEFAULT NULL,
  `from_type` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `content_lenth` varchar(255) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

SET FOREIGN_KEY_CHECKS = 1;
