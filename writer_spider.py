# !/usr/anaconda3/bin python
# -*-coding:utf-8-*-
# author:tangchaolizi
# datetime:2019-09-10 23:10
# software:PyCharm
from bs4 import BeautifulSoup
import requests


class Spider(object):
    def __init__(self, url):
        self.headers = {
            'User-Agent': r'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0',
        }
        self.url = url

    def wechat(self):
        content = requests.get(url=self.url, headers=self.headers)
        soup = BeautifulSoup(content.text, 'lxml')
        writer_soup = soup.find_all("li", attrs={"class": "fl"})
        for i in writer_soup:
            writer = i.a['href'][8:-1]
            if writer in writer_item:
                continue
            else:
                writer_item.append(writer)


if __name__ == '__main__':
    writer_item = []
    for i in range(1,87):
        url = 'https://www.6949.com/writer-bagua/p{}.html'.format(i)
        print(url)
        test = Spider(url)
        test.wechat()
    print(writer_item)
    f = open('writer.txt', 'a+')
    f.write(str(writer_item))
